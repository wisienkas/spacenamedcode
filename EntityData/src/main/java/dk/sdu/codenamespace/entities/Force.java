package dk.sdu.codenamespace.entities;

import java.util.ArrayList;
import java.util.List;

public class Force {

	public List<InnerForce> list = new ArrayList<Force.InnerForce>();
	
	public class InnerForce{
		public float x, y;
	}
	
	public void startNew(InnerForce inf){
		list = new ArrayList<Force.InnerForce>();
		list.add(inf);
	}
}
