package dk.sdu.codenamespace.entities;

public class ImageData {

	public String path;
	public float scaleX;
	public float scaleY;
	public float aplha;
}
