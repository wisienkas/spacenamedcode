package dk.sdu.codenamespace.entities;

public class World {
	
	private World() {};
	
	private static World instance;
	
	public static World getInstance() {
		if(instance == null)
			instance = new World();
		return instance;
	}

}
