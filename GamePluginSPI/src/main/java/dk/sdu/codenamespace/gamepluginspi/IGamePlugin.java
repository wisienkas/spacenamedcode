package dk.sdu.codenamespace.gamepluginspi;

import dk.sdu.codenamespace.entities.Entity;

public interface IGamePlugin {
	public void process(Entity entity);
}
