package dk.sdu.codenamespace.inputspi;

public enum EAction {
    Accelerate,
    Decelerate,
    TurnLeft,
    TurnRight
}
