package dk.sdu.codenamespace.inputspi;

public interface IInput {
	public boolean isActionActive(EAction action);
}
