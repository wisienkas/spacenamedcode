package dk.sdu.codenamespace.inputspi;

/**
 * Created by Thomas on 20-03-14.
 */
public interface IKeyboardInputListener {
    public void onKeyUp(String key);
    public void onKeyDown(String key);
}
