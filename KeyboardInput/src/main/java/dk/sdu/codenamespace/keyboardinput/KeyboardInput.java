package dk.sdu.codenamespace.keyboardinput;

import dk.sdu.codenamespace.inputspi.EAction;
import dk.sdu.codenamespace.inputspi.IInput;
import dk.sdu.codenamespace.inputspi.IKeyboardInputListener;

import java.util.HashMap;

public class KeyboardInput implements IInput, IKeyboardInputListener {

    private HashMap<EAction, String> keyMapping;
    private HashMap<EAction, Boolean> isActionActive;

    public KeyboardInput() {
        this.isActionActive = new HashMap<EAction, Boolean>();
        this.keyMapping = new HashMap<EAction, String>();

        this.initializeKeymapping();
    }

    private void initializeKeymapping() {
        // TODO load these values from settings
        this.keyMapping.put(EAction.Accelerate, "UP");
        this.keyMapping.put(EAction.Decelerate, "DOWN");
        this.keyMapping.put(EAction.TurnLeft, "LEFT");
        this.keyMapping.put(EAction.TurnRight, "RIGHT");
    }

    @Override
    public boolean isActionActive(EAction action) {
        return this.isActionActive.containsKey(action) ? this.isActionActive.get(action) : false;
    }

    @Override
    public void onKeyUp(String key) {
        this.processInput(false, key);
    }

    @Override
    public void onKeyDown(String key) {
        this.processInput(true, key);
    }

    private void processInput(boolean value, String key) {
        for (EAction action : EAction.values()) {
            if (this.keyMapping.get(action).equals(key)) {
                this.isActionActive.put(action, value);
                break;
            }
        }
    }
}
