package dk.sdu.codenamespace.spilocator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

public class SPILocator
{
	@SuppressWarnings("rawtypes")
	private static Map<Class, ServiceLoader> loadermap = new HashMap<Class, ServiceLoader>();

	private SPILocator()
	{}

	@SuppressWarnings("unchecked")
	public static <T> List<T> locate(Class<T> service)
	{
		ServiceLoader<T> loader = loadermap.get(service);

		if (loader == null)
		{
			loader = ServiceLoader.load(service);
			loadermap.put(service, loader);
		}

		List<T> list = new ArrayList<T>();
		
		if (loader != null)
		{
			try
			{
				for (T instance : loader)
				{
					list.add(instance);
				}
			}
			catch (ServiceConfigurationError serviceError)
			{
				serviceError.printStackTrace();
			}
		}
		
		System.out.println("Found " + list.size() + " implementations for interface: " + service.getName());
		
		return list;
	}
}
