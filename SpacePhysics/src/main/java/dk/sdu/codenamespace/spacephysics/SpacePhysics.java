package dk.sdu.codenamespace.spacephysics;

import static com.decouplink.Utilities.context;
import dk.sdu.codenamespace.entities.Entity;
import dk.sdu.codenamespace.entities.Force;
import dk.sdu.codenamespace.entities.Force.InnerForce;
import dk.sdu.codenamespace.entities.Position;
import dk.sdu.codenamespace.gamepluginspi.IGamePlugin;

public class SpacePhysics implements IGamePlugin{

	private final float drag = 1 - 0.03f;
	
	@Override
	public void process(Entity entity) {
		Position pos = context(entity).one(Position.class);
		
		Force forces = context(entity).one(Force.class);
		InnerForce resultForce = forces.new InnerForce();
		resultForce.x = 0;
		resultForce.y = 0;
		for (InnerForce inf : forces.list) {
			resultForce.x += inf.x;
			resultForce.y += inf.y;
		}
		pos.y += resultForce.y;
		pos.x += resultForce.x;
		resultForce.x *= this.drag;
		resultForce.y *= this.drag;
		forces.startNew(resultForce);
	}

}
