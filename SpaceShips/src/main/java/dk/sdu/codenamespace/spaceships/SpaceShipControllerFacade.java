package dk.sdu.codenamespace.spaceships;


import dk.sdu.codenamespace.inputspi.EAction;
import dk.sdu.codenamespace.inputspi.IInput;
import dk.sdu.codenamespace.spilocator.SPILocator;

public class SpaceShipControllerFacade {
	private IInput input;
	
	
	protected SpaceShipControllerFacade() {
		input = (IInput) SPILocator.locate(IInput.class).iterator().next();
	} 
	protected boolean checkSpaceShipControlAction(EAction action){
		return input.isActionActive(action);
	}

}
