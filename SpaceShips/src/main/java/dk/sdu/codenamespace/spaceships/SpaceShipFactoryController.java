package dk.sdu.codenamespace.spaceships;

import java.util.HashMap;
import java.util.Map;

import dk.sdu.codenamespace.entities.Entity;
import dk.sdu.codenamespace.spaceships.shipfactories.Alminions;
import dk.sdu.codenamespace.spaceships.shipfactories.SpaceShipFactory;

public class SpaceShipFactoryController {
	protected static enum factionEnum {ALMINIONS, EMINIONS, HALLDORIONS, THOMASIONS};
	protected Map<factionEnum, SpaceShipFactory> factoryMap;
	
	protected SpaceShipFactoryController(){
		factoryMap = new HashMap<factionEnum, SpaceShipFactory>();
		factoryMap.put(factionEnum.ALMINIONS, new Alminions());
	}
	
	protected Entity buildShip(factionEnum fac){
		return factoryMap.get(fac).buildShip();
	}
}
