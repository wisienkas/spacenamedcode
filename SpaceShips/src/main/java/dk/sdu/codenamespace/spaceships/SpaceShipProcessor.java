package dk.sdu.codenamespace.spaceships;

import java.util.HashMap;
import java.util.Map;
import dk.sdu.codenamespace.entities.Entity;
import dk.sdu.codenamespace.entities.Force;
import dk.sdu.codenamespace.entities.Force.InnerForce;
import dk.sdu.codenamespace.entities.Position;
import dk.sdu.codenamespace.inputspi.EAction;
import static com.decouplink.Utilities.context;

public class SpaceShipProcessor {
	private SpaceShipControllerFacade sscf;
	
	protected SpaceShipProcessor(){
		sscf = new SpaceShipControllerFacade();
	}
	
	protected void processShip(Entity ship){
		Force forces = context(ship).one(Force.class);
		forces.list.add(calculateForce(checkForConInput(), ship));
	}
	
	private Map<EAction, Boolean> checkForConInput(){
		Map<EAction, Boolean> inputmap = new HashMap<EAction, Boolean>();
		for(EAction e : EAction.values()){
			inputmap.put(e, sscf.checkSpaceShipControlAction(e));
		}
		return inputmap;
	}
	
	private Force.InnerForce calculateForce(Map<EAction, Boolean> inputs, Entity ship){
		Position pos = context(ship).one(Position.class);
		
		if(inputs.get(EAction.TurnLeft)){
			if(!inputs.get(EAction.TurnRight)){
				pos.angle -= 0.15f;
			}
		}else if(inputs.get(EAction.TurnRight)){
			pos.angle += 0.15f;
		}
		
		float vec_x = (float) Math.cos(pos.angle);
		float vec_y = (float) Math.sin(pos.angle);
		
		if(inputs.get(EAction.Accelerate)){
			if(!inputs.get(EAction.Decelerate)){
				vec_x *= 1;
				vec_y *= 1;
			}else{
				vec_x *= 0;
				vec_y *= 0;
			}
		}else if(inputs.get(EAction.Decelerate)){
			vec_x *= -1;
			vec_y *= -1;
		}else{
			vec_x *= 0;
			vec_y *= 0;
		}
		
		InnerForce inf = new Force().new InnerForce();
		inf.x = vec_x;
		inf.y = vec_y;
		
		return inf;
	}
}
