package dk.sdu.codenamespace.spaceships;

import dk.sdu.codenamespace.entities.Entity;
import dk.sdu.codenamespace.gamepluginspi.IGamePlugin;

public class SpaceShips implements IGamePlugin{
	private SpaceShipProcessor ssp;
	private SpaceShipFactoryController ssfc;
	
	public SpaceShips(){
		ssp = new SpaceShipProcessor();
		ssfc = new SpaceShipFactoryController();
		
		
		///////////////////////////////////////////////////////////////////////
		//						first iteration hardcoded test
		///////////////////////////////////////////////////////////////////////
		//																	 //
		ssfc.buildShip(SpaceShipFactoryController.factionEnum.ALMINIONS);
		//																	 //
		/////////////////////////////////END HARDCODE TEST/////////////////////
		
	}
	
	@Override
	public void process(Entity entity) {
		// TODO Auto-generated method stub
		ssp.processShip(entity);
	}

}
