package dk.sdu.codenamespace.spaceships.shipfactories;

import static com.decouplink.Utilities.context;

import java.util.ArrayList;

import dk.sdu.codenamespace.entities.Entity;
import dk.sdu.codenamespace.entities.Force;
import dk.sdu.codenamespace.entities.ImageData;
import dk.sdu.codenamespace.entities.Position;
import dk.sdu.codenamespace.entities.World;


public class Alminions extends SpaceShipFactory {

	@Override
	public Entity buildShip() {
		Entity ship = new Entity();
		ImageData img = new ImageData();
		img.path = "images/test.png";
		img.scaleX = 0.1f;
		img.scaleY = 0.1f;
		img.aplha = 1;
		
		Position pos = new Position();
		pos.angle = 0;
		pos.x = 10;
		pos.y = 10;
		
		Force fos = new Force();
		fos.list = new ArrayList<Force.InnerForce>();
		
		context(ship).add(Position.class, pos);
		context(ship).add(ImageData.class, img);
		context(ship).add(Force.class, fos);
		
		context(World.getInstance()).add(Entity.class, ship);
		return ship;
	}

}
