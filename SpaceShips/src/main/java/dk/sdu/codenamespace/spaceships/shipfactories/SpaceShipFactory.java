package dk.sdu.codenamespace.spaceships.shipfactories;

import dk.sdu.codenamespace.entities.Entity;

public abstract class SpaceShipFactory {
	
	public abstract Entity buildShip(); 


}
