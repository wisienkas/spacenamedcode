package dk.sdu.codenamespace.utility;

public class Vector {
	public float x, y;
	
	public Vector(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	public Vector(){
		this(0f,0f);
	}
	
	public Vector addVectors(Vector v){
		this.x += v.x;
		this.y += v.y;
		return this;
	}
	
}
