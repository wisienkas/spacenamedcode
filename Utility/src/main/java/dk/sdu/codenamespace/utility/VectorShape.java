package dk.sdu.codenamespace.utility;

public class VectorShape extends Vector {
	
	public float radius;
	
	public VectorShape(float radius, float x, float y) {
		super(x, y);
		this.radius = radius;
	}
	
	public VectorShape(){
		this(0f,0f,0f);
	}
	
	public boolean isCollision(VectorShape vector){
		float x = this.x - vector.x < 0 ? vector.x - this.x : this.x - vector.x;
		float y = this.y - vector.y < 0 ? vector.y - this.y : this.y - vector.y;
		x = x * x;
		y = y * y;
		if(Math.sqrt(x + y) < vector.radius + this.radius){
			return true;
		}
		return false;
	}
	
	public boolean isCollision(Vector vector){
		float x = this.x - vector.x < 0 ? vector.x - this.x : this.x - vector.x;
		float y = this.y - vector.y < 0 ? vector.y - this.y : this.y - vector.y;
		x = x * x;
		y = y * y;
		if(Math.sqrt(x + y) < this.radius){
			return true;
		}
		return false;
	}
}
