package dk.sdu.codenameSpace.android;

import playn.android.GameActivity;
import playn.core.PlayN;

import dk.sdu.codenameSpace.core.CodenameSpace;

public class CodenameSpaceActivity extends GameActivity {

  @Override
  public void main(){
    PlayN.run(new CodenameSpace());
  }
}
