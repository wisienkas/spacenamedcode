package dk.sdu.codenameSpace.core;

import static playn.core.PlayN.assets;
import static playn.core.PlayN.graphics;
import static com.decouplink.Utilities.context;

import java.util.Collection;
import java.util.List;

import playn.core.PlayN;
import playn.core.Game;
import playn.core.Image;
import playn.core.ImageLayer;
import playn.core.util.Clock;
import dk.sdu.codenamespace.entities.Entity;
import dk.sdu.codenamespace.entities.ImageData;
import dk.sdu.codenamespace.entities.Position;
import dk.sdu.codenamespace.entities.World;
import dk.sdu.codenamespace.gamepluginspi.IGamePlugin;
import dk.sdu.codenamespace.spilocator.SPILocator;

public class GameEngine extends Game.Default {
	
	private List<IGamePlugin> plugins;
	private final Clock.Source clock = new Clock.Source(33);

  public GameEngine() {
    super(33); // call update every 33ms (30 times per second)
  }

  @Override
  public void init()
  {
	  //Load plugins
	  plugins = SPILocator.locate(IGamePlugin.class);
	  
	  //setup world object
	  context(World.getInstance()).add(World.class, World.getInstance());
	  
    // create and add background image layer
    Image bgImage = assets().getImage("images/bg.png");
    ImageLayer bgLayer = graphics().createImageLayer(bgImage);
    graphics().rootLayer().add(bgLayer);

      //Add keyboard listener
    PlayN.keyboard().setListener(new KeyboardListener());
  }

  @Override
  public void update(int delta) 
  {
	  for(IGamePlugin p : plugins) 
		  for(Entity e : context(World.getInstance()).all(Entity.class))
			  p.process(e);
  }

  @Override
  public void paint(float alpha) 
  {
    // the background automatically paints itself, so no need to do anything here!
	  
		clock.paint(alpha);

		Collection<? extends Entity> collection = context(World.getInstance()).all(Entity.class);
		
		if (collection != null)
		{
		
		for (Entity e : collection) {
			ImageLayer view = getImageLayer(e);
			Position p = context(e).one(Position.class);
			ImageData imgData = context(e).one(ImageData.class);
			

			view.setTranslation(p.x, p.y);
			view.setRotation(p.angle);
			view.setAlpha(imgData.aplha);
			view.setScale(imgData.scaleX, imgData.scaleY);
		}
		}
		else
		{
			System.out.println("Is null");
		}
  }
  
	private ImageLayer getImageLayer(Entity entity) {

		ImageLayer image = context(entity).one(ImageLayer.class);
		
		if(image == null) 
		{
			String imagePath = context(entity).one(ImageData.class).path;
			image = graphics().createImageLayer(assets().getImageSync(imagePath));
			image.setOrigin(image.width() / 2f, image.height() / 2f);
			context(entity).add(ImageLayer.class, image);
			
			graphics().rootLayer().add(image);
		}			
		return image;
	}
}
