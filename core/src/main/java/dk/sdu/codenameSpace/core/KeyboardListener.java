package dk.sdu.codenameSpace.core;

import dk.sdu.codenamespace.inputspi.*;
import dk.sdu.codenamespace.spilocator.SPILocator;
import playn.core.Keyboard;

import java.util.List;

/**
 * Created by Thomas on 20-03-14.
 */
public class KeyboardListener extends Keyboard.Adapter {
    private IKeyboardInputListener listener;

    public KeyboardListener() {
        this.initialize();
    }

    private void initialize() {
        List<IInput> listeners = SPILocator.locate(IInput.class);

        for(IInput input : listeners)
        {
            if(input instanceof IKeyboardInputListener)
            {
                this.listener = (IKeyboardInputListener)input;
                break;
            }
        }
    }

    @Override
    public void onKeyDown(Keyboard.Event event) {
        if (this.listener != null) {
            this.listener.onKeyDown(event.key().name());
        }
    }

    @Override
    public void onKeyUp(Keyboard.Event event) {
        if (this.listener != null) {
            this.listener.onKeyUp(event.key().name());
        }
    }
}
