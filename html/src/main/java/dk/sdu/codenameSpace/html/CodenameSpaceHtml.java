package dk.sdu.codenameSpace.html;

import playn.core.PlayN;
import playn.html.HtmlGame;
import playn.html.HtmlPlatform;

import dk.sdu.codenameSpace.core.CodenameSpace;

public class CodenameSpaceHtml extends HtmlGame {

  @Override
  public void start() {
    HtmlPlatform.Config config = new HtmlPlatform.Config();
    // use config to customize the HTML platform, if needed
    HtmlPlatform platform = HtmlPlatform.register(config);
    platform.assets().setPathPrefix("codenameSpace/");
    PlayN.run(new CodenameSpace());
  }
}
