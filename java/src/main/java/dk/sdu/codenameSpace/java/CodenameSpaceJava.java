package dk.sdu.codenameSpace.java;

import playn.core.PlayN;
import playn.java.JavaPlatform;

import dk.sdu.codenameSpace.core.GameEngine;

public class CodenameSpaceJava {

  public static void main(String[] args) {
    JavaPlatform.Config config = new JavaPlatform.Config();
    // use config to customize the Java platform, if needed
    JavaPlatform.register(config);
    PlayN.run(new GameEngine());
  }
}
